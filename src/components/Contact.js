import {app} from '../main.js';
import Header from './layout/Header.js';
import Footer from './layout/Footer.js';
import Contact from './others/contact.js';

function generate() {
    // Add header to the virtual DOM
    Header.generate();

    // Add Hero to virtual DOM
    app.createElement('section', {class: 'hero is-primary'},
        app.createElement('div', {class: 'hero-body'},
            app.createElement('div', {class: 'container'},
                app.createElement('h1', {class: 'title'}, 'Personal'),
                app.createElement('h2', {class: 'subtitle'}, 'Our Favourite Place in the world')
            )
        )
    );

    // Add content to virtual DOM
    app.createElement('div', {class: 'container'},
        app.createElement('div', {class: 'section'},
            app.createElement('div', {class: 'row columns is-multiline'},
                Contact.generate('Aloïs marcellin', 'https://media-exp1.licdn.com/dms/image/C4D03AQHXpkr-VyDYTA/profile-displayphoto-shrink_200_200/0?e=1585180800&v=beta&t=zaKII56pBkv08HnbZbGtknkaHQl9CdsKjBYowk-Kovk', ['PHP', 'Laravel', 'Symfony'], 'Dev Backend','https://www.linkedin.com/in/alo%C3%AFs-marcellin/'),
                Contact.generate('ISKRANE\n' + 'Kamel', 'https://dl.reseau-ges.fr/public/dcj6AO1JD5_t-PdBOWigoNFH7L_2ssdXx_KvxH9QFt4?pfdrid_c=true', ['PHP', 'Laravel', 'Symfony'], 'Dev Backend','https://www.linkedin.com/in/kamel-iskrane-15072a143/'),
                Contact.generate('FAIZEAU Alexis', 'https://dl.reseau-ges.fr/public/dcj6AO1JD5_t-PdBOWigoPEYRyiQuJv4x_KvxH9QFt4?pfdrid_c=true', ['PHP', 'Laravel', 'Symfony'], 'Dev Backend','https://www.linkedin.com/in/alexis-faizeau/'),
                Contact.generate('LE\n' + 'Christophe', 'https://dl.reseau-ges.fr/public/dcj6AO1JD5_t-PdBOWigoGQzCn7Co5Tzx_KvxH9QFt4?pfdrid_c=true', ['PHP', 'Laravel', 'Symfony'], 'Dev Backend','https://www.linkedin.com/in/christophe-le/'),
            )
        )
    );
    // Add footer to the virtual DOM
    Footer.generate();
}

export default {
    generate,
}
