import { app } from '../main.js';
import Header from './layout/Header.js';
import Footer from './layout/Footer.js';
import CityCard from './others/CityCard.js';

function generate() {
    // Add header to the virtual DOM
    Header.generate();

    // Add Hero to virtual DOM
    app.createElement('section', { class:'hero is-primary' },
        app.createElement('div', { class:'hero-body' },
            app.createElement('div', { class:'container' },
                app.createElement('h1', { class:'title' }, 'Asia'),
                app.createElement('h2', { class:'subtitle' }, '')
            )
        )
    );

    // Add content to virtual DOM
    app.createElement('div', { class:'container' },
        app.createElement('div', { class:'section' },
            app.createElement('div', { class:'row columns is-multiline' },
                CityCard.generate('Pékin,cn'),
                CityCard.generate('Achgabat,tm'),
                CityCard.generate('Amman,jo'),
                CityCard.generate('Ankara,tr'),
                CityCard.generate('Bagdad,iq'),
                CityCard.generate('Bangkok,th'),
                CityCard.generate('Damas,sy'),
                CityCard.generate('Doha,qa'),
                CityCard.generate('Jakarta,id'),
                CityCard.generate('Kuala Lumpur,my'),
                CityCard.generate('Singapour,sg'),
                CityCard.generate('Tokyo,jp')
            )
        )
    );

    // Add footer to the virtual DOM
    Footer.generate();
}

export default {
    generate,
}
