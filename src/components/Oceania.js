import { app } from '../main.js';
import Header from './layout/Header.js';
import Footer from './layout/Footer.js';
import CityCard from './others/CityCard.js';

function generate() {
    // Add header to the virtual DOM
    Header.generate();

    // Add Hero to virtual DOM
    app.createElement('section', { class:'hero is-primary' },
        app.createElement('div', { class:'hero-body' },
            app.createElement('div', { class:'container' },
                app.createElement('h1', { class:'title' }, 'Oceania'),
                app.createElement('h2', { class:'subtitle' }, '')
            )
        )
    );

    app.createElement('div', { class:'container' },
        app.createElement('div', { class:'section' },
            app.createElement('div', { class:'row columns is-multiline' },
                CityCard.generate('Canberra,au'),
                CityCard.generate('Suva,fj'),
                CityCard.generate('Wellington,nz'),
                CityCard.generate('Port Moresby,pg'),
                CityCard.generate('Nuku’alofa,to'),
                CityCard.generate('Funafuti,tv'),
                CityCard.generate('Port-Vila, vu'),
            )
        )
    );

    // Add footer to the virtual DOM
    Footer.generate();
}

export default {
    generate,
}
