import { app } from '../main.js';
import Header from './layout/Header.js';
import Footer from './layout/Footer.js';
import CityCard from './others/CityCard.js';

function generate() {
    // Add header to the virtual DOM
    Header.generate();

    // Add Hero to virtual DOM
    app.createElement('section', { class:'hero is-primary' },
        app.createElement('div', { class:'hero-body' },
            app.createElement('div', { class:'container' },
                app.createElement('h1', { class:'title' }, 'America'),
                app.createElement('h2', { class:'subtitle' }, '')
            )
        )
    );

    // Add content to virtual DOM
    app.createElement('div', { class:'container' },
        app.createElement('div', { class:'section' },
            app.createElement('div', { class:'row columns is-multiline' },
                CityCard.generate('New York,us'),
                CityCard.generate('Los Angeles,us'),
                CityCard.generate('Bogota,co'),
                CityCard.generate('Brasilia,br'),
                CityCard.generate('Buenos Aires,ar'),
                CityCard.generate('Caracas,ve'),
                CityCard.generate('Guatemala City,gt'),
                CityCard.generate('La Havane,cu'),
                CityCard.generate('Kingston,jm'),
                CityCard.generate('Lima,pe'),
                CityCard.generate('Mexico,mx'),
                CityCard.generate('Montevideo,uy'),
                CityCard.generate('Nassau,bs'),
                CityCard.generate('Ottawa,ca'),
                CityCard.generate('La Paz,bo')
            )
        )
    );

    // Add footer to the virtual DOM
    Footer.generate();
}

export default {
    generate,
}
