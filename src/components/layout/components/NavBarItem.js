import { app } from '../../../main.js';

function generate(routeName, title) {
    let path = app.router.getPathByRouteName(routeName);
    let props = {
        class: 'navbar-item',
        href: path
    };

    if (app.router.isCurrentUri(path)) {
        props.class = props.class + ' is-active';
    }

    return app.createElement('a', props, title);
}

export default {
    generate,
};
