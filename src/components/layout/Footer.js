import { app } from '../../main.js';

function generate() {
    return app.createElement('footer', { class:'footer' },
        app.createElement('div', { class:'container' },
            app.createElement('div', { class:'content has-text-centered' },
                app.createElement('p', {},
                    "WhatWeather code with ",
                    app.createElement('i', { class:'fas fa-heart' }),
                    ' & ',
                    app.createElement('i', { class:'fas fa-coffee' })
                )
            )
        )
    );
}

export default {
    generate,
};
