import {app} from '../../main.js';
import NavBarItem from './components/NavBarItem.js';

function generate() {
    return app.createElement('nav', {class: 'navbar', role: 'navigation', 'aria-label': 'main navigation'},
        app.createElement('div', {class: 'container'},
            app.createElement('div', {class: 'navbar-brand'},
                app.createElement('a', {class: 'navbar-item', href: app.router.getPathByRouteName('home')},
                    app.createElement('p', {}, 'WhatWheather ?'),
                ),
                app.createElement('span', {class: 'navbar-burger burger', 'data-target': 'navbarMenu'},
                    app.createElement('span', {}),
                    app.createElement('span', {}),
                    app.createElement('span', {})
                )
            ),
            app.createElement('div', {id: 'navbarMenu', class: 'navbar-menu'},
                app.createElement('div', {class: 'navbar-end'},
                    NavBarItem.generate('home', 'Home'),
                    NavBarItem.generate('world', 'World'),
                    NavBarItem.generate('europe', 'Europe'),
                    NavBarItem.generate('america', 'America'),
                    NavBarItem.generate('asia', 'Asia'),
                    NavBarItem.generate('africa', 'Africa'),
                    NavBarItem.generate('oceania', 'Oceania'),
                    NavBarItem.generate('Contact', 'Contact')
                )
            )
        )
    );
}

function navListner() {
    document.addEventListener('DOMContentLoaded', () => {
        // Get all "navbar-burger" elements
        const $navbarBurgers = Array.prototype.slice.call(document.querySelectorAll('.navbar-burger'), 0);
        // Check if there are any navbar burgers
        if ($navbarBurgers.length > 0) {
            // Add a click event on each of them
            $navbarBurgers.forEach(el => {
                el.addEventListener('click', () => {
                    // Get the target from the "data-target" attribute
                    const target = el.dataset.target;
                    const $target = document.getElementById(target);
                    // Toggle the "is-active" class on both the "navbar-burger" and the "navbar-menu"
                    el.classList.toggle('is-active');
                    $target.classList.toggle('is-active');
                });
            });
        }
    });
}

export default {
    generate,
    navListner,
};
