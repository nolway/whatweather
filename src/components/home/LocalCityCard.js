import { app } from '../../main.js';
import Config from '../../config.js';

let cardContent = null;

function generate(isFullWidth) {
    cardContent = app.createElement('div', { class:'card large is-full' },
        app.createElement('div', { class:'card-content' },
            app.createElement('div', { class:'content has-text-centered' },
                app.createElement('i', { class:'fas fa-spinner fa-pulse' })
            )
        )
    );

    let weather = getWeather();
    return app.createElement('div', { class: `column ${isFullWidth ? 'is-full' : 'is-one-third'}`},
        cardContent
    );
}

function getWeather(){
    let props = cardContent.getProps();
    if (!navigator.geolocation) {
        props.children = [
            app.createElement('div', { class:'card-content' },
                app.createElement('div', { class:'content' },
                    app.createElement('p', { class:'has-text-danger' },
                        'Geolocation is not supported by this browser.'
                    )
                )
            )
        ];

        cardContent.display(props);
        return;
    }

    navigator.geolocation.getCurrentPosition(getWeatherByPosition, getWeatherByPositionError, cardContent);
}

function getWeatherByPosition(position) {
    let request = new XMLHttpRequest();
    request.open('GET', 'https://api.openweathermap.org/data/2.5/weather?lat=' + position.coords.latitude + '&lon=' + position.coords.longitude + '&appid=' + Config.openWeatherKey, true);
    request.onload = function() {
        let data = JSON.parse(this.response);
        let props = cardContent.getProps();

        if (request.status >= 200 && request.status < 400 && data.cod === 200) {
            data.weather = data.weather[0];

            props.children = [
                app.createElement('header', { class:'card-header' },
                    app.createElement('p', { class:'card-header-title' },
                        data.name,
                        ', ',
                        data.sys.country,
                        app.createElement('span', { style:'margin-left:5px', class:'flag-icon flag-icon-' + data.sys.country.toLowerCase() })
                    )
                ),
                app.createElement('div', { class:'card-content' },
                    app.createElement('div', { class:'content' },
                        app.createElement('div', { class: 'columns' },
                            app.createElement('div', { class:'weather column' },
                                app.createElement('p', { },
                                    data.weather.main + ' ',
                                    app.createElement('img', { src:'http://openweathermap.org/img/wn/' + data.weather.icon + '@2x.png' })
                                ),
                                app.createElement('p', { },
                                    data.weather.description
                                )
                            ),
                            app.createElement('hr', { }),
                            app.createElement('div', { class:'temperature column' },
                                app.createElement('p', { },
                                    app.createElement('ul', { },
                                        app.createElement('li', { },
                                            'Current : ' + kelvinToCelsius(data.main.temp) + ' °c'
                                        ),
                                        app.createElement('li', { },
                                            'Min : ' + kelvinToCelsius(data.main.temp_min) + ' °c'
                                        ),
                                        app.createElement('li', { },
                                            'Max : ' + kelvinToCelsius(data.main.temp_max) + ' °c'
                                        )
                                    )
                                )
                            ),
                            app.createElement('hr', { }),
                            app.createElement('div', { class:'others-data column' },
                                app.createElement('p', { },
                                    app.createElement('ul', { },
                                        app.createElement('li', { },
                                            'Presure : ' + data.main.pressure + ' hpa'
                                        ),
                                        app.createElement('li', { },
                                            'Humidity : ' + data.main.humidity + ' %'
                                        ),
                                        app.createElement('li', { },
                                            'Wind speed : ' + data.wind.speed + ' m/sec'
                                        ),
                                        app.createElement('li', { },
                                            'Sunrise : ' + _formatHourByTimestamp(data.sys.sunrise)
                                        ),
                                        app.createElement('li', { },
                                            'Sunset : ' + _formatHourByTimestamp(data.sys.sunset)
                                        )
                                    )
                                )
                            )
                        )
                    )
                )
            ];
        } else {
            props.children = [
                app.createElement('div', { class:'card-content' },
                    app.createElement('div', { class:'content' },
                        app.createElement('p', { class:'has-text-danger' },
                            'The city name that you typed not exist !'
                        )
                    )
                )
            ];
        }

        cardContent.display(props);
    };

    request.send();
}

function getWeatherByPositionError(e) {
    let props = cardContent.getProps();
    props.children = [
        app.createElement('div', { class:'card-content' },
            app.createElement('div', { class:'content' },
                app.createElement('p', { class:'has-text-danger' },
                    'Something was wrong with geolocation, try again later.'
                )
            )
        )
    ];

    cardContent.display(props);
}

function kelvinToCelsius(kelvin) {
    return Math.round(kelvin - 273.15);
}

function _formatHourByTimestamp(timestamp) {
    let date = new Date(timestamp * 1000);
    return _addZero(date.getHours()) + ':' + _addZero(date.getMinutes());
}

function _addZero(value) {
    if (value < 10) {
        value = '0' + value;
    }
    return value;
}

export default {
    generate,
};
