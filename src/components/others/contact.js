import {app} from '../../main.js';
import Config from '../../config.js';

function generate(name, image, tech, post, link) {
    let cardContent = app.createElement('div', {class: 'card large'},
        app.createElement('div', {class: 'card-content'},
            app.createElement('div', {class: 'content has-text-centered'},
                app.createElement('i', {class: 'fas fa-spinner fa-pulse'})
            )
        )
    );

    let weather = getWeather(name, image, tech, post, link, cardContent);
    return app.createElement('div', {class: `column is-one-third`},
        cardContent
    );
}

function getWeather(name, image, tech, post, link, cardContent) {
    let request = new XMLHttpRequest();
    request.open('GET', 'https://api.openweathermap.org/data/2.5/weather?q=' + name + '&appid=' + Config.openWeatherKey, true);
    request.onload = function () {
        let data = JSON.parse(this.response);
        let props = cardContent.getProps();


        var techo = "";
        for (var i = 0; i < tech.length; i++) {
            if (i != 0) {
                techo += ", ";
            }
            techo += tech[i];
        }
        props.children = [
            app.createElement('header', {class: 'card-header'},
                app.createElement('p', {class: 'card-header-title is-size-5'},
                    name)
            ),
            app.createElement('div', {class: 'card-content'},
                app.createElement('div', {class: 'content'},
                    app.createElement('div', {class: 'weather'},
                        app.createElement('h3', {class: 'is-size-6'},
                            'Activité'
                        ),
                        app.createElement('p', {},
                            app.createElement('img', {src: image})
                        ),
                        app.createElement('p', {},
                            'Dev Backend'
                        )
                    ),
                    app.createElement('hr', {}),
                    app.createElement('div', {class: 'temperature'},
                        app.createElement('h3', {class: 'is-size-6'},
                            'Techno : '
                        ),
                        app.createElement('p', {},
                            app.createElement('ul', {},
                                techo
                            )
                        )
                    ),
                    app.createElement('hr', {}),
                    app.createElement('div', {class: 'temperature'},
                        app.createElement('h3', {class: 'is-size-6'},
                            'Linkedin : '
                        ),
                        app.createElement('p', {},
                            app.createElement('a', {
                                    class: 'button is-primary',
                                    target: "_blank",
                                    href: link,
                                    onclick: `window.open("${link}");`
                                },
                                'Linkedin'
                            )
                        ),
                    ),
                )
            )
        ];


        cardContent.display(props);
    };

    request.send();
}

export default {
    generate,
};
