import { app } from '../../main.js';
import Config from '../../config.js';

function generate(city, isFullWidth) {
    let cardContent = app.createElement('div', { class:'card large' },
        app.createElement('div', { class:'card-content' },
            app.createElement('div', { class:'content has-text-centered' },
                app.createElement('i', { class:'fas fa-spinner fa-pulse' })
            )
        )
    );

    let weather = getWeather(city, cardContent);
    return app.createElement('div', { class: `column ${isFullWidth ? 'is-full' : 'is-one-third'}`},
        cardContent
    );
}

function getWeather(city, cardContent){
    let request = new XMLHttpRequest();
    request.open('GET', 'https://api.openweathermap.org/data/2.5/weather?q=' + city + '&appid=' + Config.openWeatherKey, true);
    request.onload = function() {
        let data = JSON.parse(this.response);
        let props = cardContent.getProps();

        if (request.status >= 200 && request.status < 400 && data.cod === 200) {
            data.weather = data.weather[0];

            props.children = [
                app.createElement('header', { class:'card-header' },
                    app.createElement('p', { class:'card-header-title is-size-5' },
                        data.name,
                        ', ',
                        data.sys.country,
                        app.createElement('span', { style:'margin-left:5px', class:'flag-icon flag-icon-' + data.sys.country.toLowerCase() })
                    )
                ),
                app.createElement('div', { class:'card-content' },
                    app.createElement('div', { class:'content' },
                        app.createElement('div', { class:'weather' },
                            app.createElement('h3', { class:'is-size-6' },
                                'Weather'
                            ),
                            app.createElement('p', { },
                                data.weather.main + ' ',
                                app.createElement('img', { src:'http://openweathermap.org/img/wn/' + data.weather.icon + '@2x.png' })
                            ),
                            app.createElement('p', { },
                                data.weather.description
                            )
                        ),
                        app.createElement('hr', { }),
                        app.createElement('div', { class:'temperature' },
                            app.createElement('h3', { class:'is-size-6' },
                                'Temperature'
                            ),
                            app.createElement('p', { },
                                app.createElement('ul', { },
                                    app.createElement('li', { },
                                        'Current : ' + kelvinToCelsius(data.main.temp) + ' °c'
                                    ),
                                    app.createElement('li', { },
                                        'Min : ' + kelvinToCelsius(data.main.temp_min) + ' °c'
                                    ),
                                    app.createElement('li', { },
                                        'Max : ' + kelvinToCelsius(data.main.temp_max) + ' °c'
                                    )
                                )
                            )
                        ),
                        app.createElement('hr', { }),
                        app.createElement('div', { class:'others-data' },
                            app.createElement('p', { },
                                app.createElement('ul', { },
                                    app.createElement('li', { },
                                        'Presure : ' + data.main.pressure + ' hpa'
                                    ),
                                    app.createElement('li', { },
                                        'Humidity : ' + data.main.humidity + ' %'
                                    ),
                                    app.createElement('li', { },
                                        'Wind speed : ' + data.wind.speed + ' m/sec'
                                    ),
                                    app.createElement('li', { },
                                        'Time zone : UTC+' + _formatTimeZone(data.timezone)
                                    ),
                                    app.createElement('li', { },
                                        'Sunrise : ' + _formatHourByTimestamp(data.sys.sunrise)
                                    ),
                                    app.createElement('li', { },
                                        'Sunset : ' + _formatHourByTimestamp(data.sys.sunset)
                                    )
                                )
                            )
                        )
                    )
                )
            ];
        } else {
            props.children = [
                app.createElement('div', { class:'card-content' },
                    app.createElement('div', { class:'content' },
                        app.createElement('p', { class:'has-text-danger' },
                            'The city name that you typed not exist !'
                        )
                    )
                )
            ];
        }

        cardContent.display(props);
    };

    request.send();
}

function kelvinToCelsius(kelvin) {
    return Math.round(kelvin - 273.15);
}

function _formatHourByTimestamp(timestamp) {
    let date = new Date(timestamp * 1000);
    return _addZero(date.getHours()) + ':' + _addZero(date.getMinutes());
}

function _addZero(value) {
    if (value < 10) {
        value = '0' + value;
    }
    return value;
}

function _formatTimeZone(minutes){
    return (minutes == 0) ? 0 : (minutes / 3600);
}

export default {
    generate,
};
