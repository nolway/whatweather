export default [
  {
    name: 'home',
    path: '/',
    component: 'Home'
  },
  {
    name: 'city',
    path: '/city',
    component: 'City',
    title: 'City'
  },
  {
    name: 'world',
    path: '/world',
    component: 'World',
    title: 'World'
  },
  {
    name: 'europe',
    path: '/europe',
    component: 'Europe',
    title: 'Europe'
  },
  {
    name: 'america',
    path: '/america',
    component: 'America',
    title: 'America'
  },
  {
    name: 'asia',
    path: '/asia',
    component: 'Asia',
    title: 'Asia'
  },
  {
    name: 'africa',
    path: '/africa',
    component: 'Africa',
    title: 'Africa'
  },
  {
    name: 'oceania',
    path: '/oceania',
    component: 'Oceania',
    title: 'Oceania'
  },
  {
    name: 'Contact',
    path: '/Contact',
    component: 'Contact',
    title: 'Contact'
  }
];