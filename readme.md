# WhatWheather
Show all around the world weather in real time !

## Installation

Use Docker & Docker Compose to install.

```bash
docker-compose up -d
```

Go to [OpenWeatherMap](https://openweathermap.org/), create an account and get your API key.

Copy /src/config.js.dist to /src/config.js and put it your key.
